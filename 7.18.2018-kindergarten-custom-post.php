<?php

/**
 * Plugin Name: Kindergarten Custom Post
 * Description: Simple Plugin to generate custom posts for Kindergarten Type and Certificate
 * Plugin URI: http://www.marlax.com
 * Author: Ratan Mia
 * Author URI: http://marlax.com/our-team/ratan-mia/
 * Version: 1.0.0
 * License: GPL2
 * Text Domain: sir-custom-post
 * Domain Path: /lang
 */

/*
    Copyright (C) Year  Author  Email

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

//Get GAN post Type (which are kindergartens)

function kindergarten_post_shortcode(){
 
 global $current_user; wp_get_current_user();
 if ( is_user_logged_in() ) { 
$user_id =  $current_user->user_login;
} 
else { 
	wp_loginout();
	 }    

// $user_id = get_current_user_id();

$args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'gan',
	'meta_key'		=> 'rank_member',
	'meta_value'	=> $user_id
);

// query
$the_query = new WP_Query( $args );

?>
<?php if( $the_query->have_posts() ): ?>

	<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>

			<div style="text-align:center"> <a  class="certificate-name" target="_blank" href="<?php the_permalink(); ?>"> 
                                                <?php the_title(); ?>
			</a> </div>

	<?php endwhile; ?>

<?php endif; ?>

<?php wp_reset_query();	 // Restore global post data stomped by the_post().

}

add_shortcode( 'kindergarten-post', 'kindergarten_post_shortcode' );


//Convert ACF Repeater Field to Wp meta

add_filter('acf/save_post', 'convert_teuda_gan_member_to_standard_wp_meta', 20);
 
function convert_teuda_gan_member_to_standard_wp_meta($post_id) {

  // Declare Member Meta Key
  $member_meta_key = 'teuda_gan_member_wp';
  delete_post_meta($post_id, $member_meta_key);
  $member_saved_values = array();

// Declare Participants Meta Key
  $participants_meta_key = 'teuda_gan_participants_wp';
  delete_post_meta($post_id, $participants_meta_key);
  $participants_saved_values = array();
  
  if (have_rows('teuda_member_group', $post_id)) {
    while (have_rows('teuda_member_group', $post_id)) {
      the_row();
      
      // Teuda Gan Member 
      $teuda_gan_member = get_sub_field('teuda_gan_member');
      if (isset($member_saved_values[$teuda_gan_member])) {
      
        continue;
      }
     add_post_meta($post_id, $member_meta_key, $teuda_gan_member, false);
     
     $member_saved_values[$teuda_gan_member] = $teuda_gan_member;
     
      
      // Teuda Gan Participants 
      $teuda_gan_participants = get_sub_field('teuda_gan_participants');
      if (isset($participants_saved_values[$teuda_gan_participants])) {
      
        continue;
      }
     add_post_meta($post_id, $participants_meta_key, $teuda_gan_participants, false);
     
     $participants_saved_values[$teuda_gan_participants] = $teuda_gan_participants;
       
    } // end while have rows
  } // end if have rows
} // end function
 

//Get Certificate post Type

function teuda_certificate_post_shortcode(){
 
 global $current_user; wp_get_current_user();
 if ( is_user_logged_in() ) { 
$user_id =  $current_user->user_login;
} 
else { 
	wp_loginout();
	 }    

// $user_id = get_current_user_id();

$args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'teudot',
	// 'meta_key'		=> 'teuda_gan_member',
	// 'meta_value'	=> $user_id
);

// query
$the_query = new WP_Query( $args );

?>
<?php if( $the_query->have_posts() ): ?>
	<ul style="list-style:none;">
	<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<li>
			
			<a href="<?php the_permalink(); ?>">
			  <h1><?php the_title(); ?></h1>
			</a>
				</li>
			         <table>
						<caption>Group of participants</caption>
						<thead>
							<tr>
								<th>Certificate Member</th>
								<th>Certificate Participants line</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
								 <?php $teuda_members = get_post_meta( get_the_ID(), 'teuda_gan_member_wp' ); 
									foreach ($teuda_members as $key => $teuda_member) {
										$user_id      = get_current_user_id();
								        $current_user = array("current_user"=>$user_id);
								        $target_user  = array_intersect($teuda_member,$current_user);
										foreach ($target_user as $key => $member) {

										  $user_info = get_userdata($member);
                                           echo "<ul style='list-style:none;'><li>".$user_info->display_name."</li></ul>";
										}
									 	
									}	
								?>
								</td>
								<td>
								 <?php $teuda_participants= get_post_meta( get_the_ID(), 'teuda_gan_participants_wp' ); 
									foreach ($teuda_participants as $key => $teuda_participant) {
									 	echo "<ul style='list-style:none;'><li>".$teuda_participant."</li></ul>";
									}
	
								?>
								</td>
							</tr>

						</tbody>
					</table>

<?php endwhile; ?>
	</ul>
<?php endif; ?>	
<?php wp_reset_query();	 // Restore global post data stomped by the_post().
}
add_shortcode( 'teuda-certificate', 'teuda_certificate_post_shortcode' );

//Get teudot post Type (which are Certificates)

function certificate_post_card_func(){
 
 global $current_user; wp_get_current_user();
 if ( is_user_logged_in() ) { 
$user_id =  $current_user->user_login;
} 
else { 
	wp_loginout();
	 }    

// $user_id = get_current_user_id();

$args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'teudot',
	'meta_key'		=> 'rank_member',
	'meta_value'	=> $user_id
);
// query
$the_query = new WP_Query( $args );

?>
<?php if( $the_query->have_posts() ): ?>
    <?php the_title(); ?>
	<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>

			<div style="text-align:center"> <a  class="certificate-name" target="_blank" href="<?php the_permalink(); ?>"> 
                                                VIEW CERTIFICATE
			</a> </div>

	<?php endwhile; ?>

<?php endif; ?>

<?php wp_reset_query();	 // Restore global post data stomped by the_post().

}
add_shortcode( 'certificate-post-card', 'certificate_post_card_func' );

// Teuda Post Meta Shortcode
function teuda_custom_post_meta(){
	?>
	<table>
						<caption>Group of participants</caption>
						<thead>
							<tr>
								<th>Certificate Member</th>
								<th>Certificate Participants line</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
								 <?php $teuda_members = get_post_meta( get_the_ID(), 'teuda_gan_member_wp' ); 

									foreach ($teuda_members as $key => $teuda_member) {
										foreach ($teuda_member as $key => $member) {

										  $user_info = get_userdata($member);
                                           echo "<ul style='list-style:none;'><li>".$user_info->display_name."</li></ul>";
										}
									 	
									}	
								?>
								</td>
								<td>
								 <?php $teuda_participants= get_post_meta( get_the_ID(), 'teuda_gan_participants_wp' ); 
									
									foreach ($teuda_participants as $key => $teuda_participant) {
									 	echo "<ul style='list-style:none;'><li>".$teuda_participant."</li></ul>";
									}	
								?>
								</td>
							</tr>

						</tbody>
					</table>
					<?php
}

add_shortcode('teuda-custom-meta','teuda_custom_post_meta');

//Textarea Area Field
function teuda_participants_list(){
	
	if( get_field('teuda_members_participants_list') ) : ?>
         
	<?php 
	$participants_obj          = get_field_objects();
	$participants_textarea = $participants_obj['teuda_members_participants_list'];
	$participants_list = get_field('teuda_members_participants_list');?>
	<?php endif; 
    $participants_arr = explode(',',$participants_list);
    $participants_list_array = array($participants_arr,$participants_textarea);
    // echo"<pre>";
    // print_r($participants_list_array);
    // echo "</pre>";
}   
add_shortcode('participants-list','teuda_participants_list');